import Vue from 'vue';


export const codeSnippet = Vue.extend({
    template: `
        <div class="sg-overlay">
            <button @click="$emit('close')">X</button>
            <code class="sg-sample"><slot>{{code}}</slot></code>
        </div>`,

    props: [ 'code' ]
});
