import Vue from 'vue';
import VueRouter from 'vue-router';
import { ministore } from 'stylegud-ui/src/plugins/ministore.js';
import { entry } from 'stylegud-ui/src/components/entry.js';

// Import the styles SCSS to make sure it gets rendered on the page
import 'css/css.scss';

// Get the data
import { sections } from '../tmp/stylegud.js';

// Init plugins
Vue.use(VueRouter);
Vue.use(ministore(sections));

// Allows you to set a wrapper class
window.__style_base_class__ = 'sample';

const router = new VueRouter({
    routes: [
        {
            name: 'currentSectionUri',
            path: '/:currentSectionUri?',
            component: entry,
            props: true,
        },
    ],
});

window.onload = function() {
    window.styleguideView = new Vue({
        router,
        el: 'div',
        data: {
            header: 'A Cool Styleguide',
            initialSectionRef: 'sample',
        },
        template: '<router-view v-bind="this.$data"></router-view>',
    });
};
