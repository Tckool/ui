# About

Provides a simple front-end integration for stylegud output.

# Samples

See the [basic integration](/samples/basic) for an example of using Stylegud UI
