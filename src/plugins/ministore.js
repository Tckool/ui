import { objectFromArrayByKey } from '../util/util.js';


export const ministore = (data) => ({
    install(Vue) {
        const byRef = objectFromArrayByKey('reference', data);
        Vue.prototype.$sectionByRef = (ref) => byRef[ref];
        Vue.prototype.$sectionsByRef = (refs) => refs.map((ref) => byRef[ref]);

        const byUri = objectFromArrayByKey('referenceURI', data);
        Vue.prototype.$sectionByUri = (ref) => byUri[ref];
        Vue.prototype.$sectionsByUri = (refs) => refs.map((ref) => byUri[ref]);
    }
});
