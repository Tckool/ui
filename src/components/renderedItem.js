import Vue from 'vue';
import { codeSnippet } from './codeSnippet';


const renderContainerClass = 'sg-render-container';
export const renderedItem = Vue.extend({
    template: `
        <div>
            <h4 v-if="renderer.getTitle()">{{renderer.getTitle()}}</h4>
            <button @click="togglePreview" class="sg-show-snippet">
                <div :class="classes"></div>
            </button>
            <code-snippet v-show="showPreview" @close="togglePreview" :code="renderer.getMarkup()"></code-snippet>
        </div>`,

    props: [ 'renderer' ],

    data() {
        const classes = {
            'sg-render-container': true,
            // eslint-disable-next-line no-undef
            [window.__style_base_class__]: Boolean(window.__style_base_class__)
        };
        return {
            classes,
            showPreview: false
        };
    },

    mounted() {
        this.renderer.renderItem(this.$el.querySelector(`.${renderContainerClass}`));
    },

    methods: {
        togglePreview() {
            this.showPreview = !this.showPreview;
        }
    },

    components: {
        codeSnippet
    }
});
